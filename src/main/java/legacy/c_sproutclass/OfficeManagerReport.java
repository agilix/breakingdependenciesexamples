package legacy.c_sproutclass;

import java.util.ArrayList;
import java.util.List;

public class OfficeManagerReport {

    public String htmlStatement(OfficeManager officeManager) {
        List<String> records = getDatabaseRecordsFor(officeManager);

        StringBuilder result = new StringBuilder();
        result.append("<table>");
        String header = String.format("<h1>Office Manager Report for <em>%s</em></h1>", officeManager.getName());
        result.append(header);

        for (String record : records) {
            result.append(String.format("<p>%s</p>", record));
        }

        String footer = "<p>------------------ 2019 (c) -------------------</p>";
        result.append(footer);
        result.append("</table>");

        return result.toString();
    }

    private List<String> getDatabaseRecordsFor(OfficeManager officeManager) {
        return new ArrayList<>();
    }

}

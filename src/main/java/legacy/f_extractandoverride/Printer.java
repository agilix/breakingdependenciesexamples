package legacy.f_extractandoverride;

public class Printer {
    public void printReceipt(ShoppingCart shoppingCart) {
        System.out.println("-----Receipt------");
        System.out.println("------------------");

        Terminal terminal = new Terminal();
        terminal.PrintLine("<<Shopping cart items>>");

        System.out.println("-----Total--------");
        System.out.println("------ " + shoppingCart.totalPrice() + " --------\n");
    }
}

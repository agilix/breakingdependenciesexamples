package legacy.d_wrapmethod;

import java.util.List;

public class Employee {

    private final int hourlyPayRate;

    public Employee() {
        hourlyPayRate = 35;
    }

    public void pay(List<TimeEntry> timeEntries, PaymentService paymentService) {

        Money totalAmount = new Money();
        for (TimeEntry entry : timeEntries) {
            int amount = entry.getHours() * hourlyPayRate;
            totalAmount.add(amount);
        }

        paymentService.pay(this, totalAmount);
    }

}

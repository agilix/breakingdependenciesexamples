package legacy.g_adaptparameter;


import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class ARMDispatcher {

    private String pageStateName;
    public final Map<String, String> marketBindings = new HashMap<>();

    ///

    public void populate(HttpServletRequest request) {
        String[] values = request.getParameterValues(pageStateName);
        if (values != null && values.length > 0) {
            marketBindings.put(pageStateName + getDateStamp(), values[0]);
        }
        /// ...
    }

    ///

    ///
    private String getDateStamp() {
        return null;
    }
}






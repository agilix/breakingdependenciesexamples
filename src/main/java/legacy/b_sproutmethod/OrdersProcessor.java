package legacy.b_sproutmethod;


import java.util.ArrayList;
import java.util.List;

public class OrdersProcessor {
    private List<Order> orders;

    public OrdersProcessor(List<Order> orders) {
        this.orders = orders;
    }

    public List<Order> getAvailableOrdersFor(Customer customer) {

        List<Order> result = new ArrayList<>();

        for (Order order : orders) {

            // some another cryptic logic
            order.recalculate();
            result.add(order);
        }

        return result;
    }

}
